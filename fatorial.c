#include "fatorial.h"

int fatorial(int x){
	/* Escreva seu código aqui */
	int final;
	if (x <= 0) {
		return -1;
	}
	else if (x == 1) {
		return 1;
	}
	else {
		final=x;
		while (x != 1) {
			final = final * (x-1);
			--x; 
		}
		return final;
	}
	
}